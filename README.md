# Voorbeeld Java applicatie met MVC en database (DAL)
Voorbeelden van het opbouwen van een applicatie met behulp van:


## Applicatie zonder database
Alleen Model klassen zonder database. Gegevens blijven in memory.

**Branch main**

## Applicatie met database - UUID als string opgeslagen
Model klassen met gegenereerde UUID. UUID wordt opgeslagen als string 
in de database. 
Voordeel is de eenvoud van code, makkelijker te debuggen: UUID in 
database is makkelijk leesbaar. Nadeel is veel dataopslag in database. 

**Branch feature_database**

## Applicatie met database - UUID binary opgeslagen
Model klassen met gegenereerde UUID. UUID wordt binary opgeslagen in de 
database. Voordeel is minder opslag in database, snelere en betere 
indexering in database. Nadeel: extra code voor 
omzetting en moeilijker leesbaar in de database. 

**Branch feature_database_binary**

## Applicatie met database - ID wordt door db-engine gegenereerd
Model klassen met id. ID wordt gegenereerd door de database engine. Na een insert
wordt de id opgevraagd en in het object gezet. 

**Branch feature_database_dbID**


