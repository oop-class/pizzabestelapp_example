package model;

public class Klant
{
	private String naam;
	private String adres;
	private String mobielnr;
	
	public String getNaam()
	{
		return naam;
	}

	public void setNaam(String naam)
	{
		this.naam = naam;
	}

	public String getAdres()
	{
		return adres;
	}

	public void setAdres(String adres)
	{
		this.adres = adres;
	}

	public String getMobielnr()
	{
		return mobielnr;
	}

	public void setMobielnr(String mobielnr)
	{
		this.mobielnr = mobielnr;
	}
}
