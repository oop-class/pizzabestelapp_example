package model;

public class Pizza
{
	private String naam;
	private double prijs;

	public Pizza(String naam, double prijs)
	{
		this.naam = naam;
		this.prijs = prijs;
	}
	
	public String getNaam()
	{
		return naam;
	}

	public void setNaam(String naam)
	{
		this.naam = naam;
	}

	public double getPrijs()
	{
		return prijs;
	}

	public void setPrijs(double prijs)
	{
		this.prijs = prijs;
	}

	public String toString()
	{
		return String.format("%s - %.2f", naam, prijs);
	}
	
}
