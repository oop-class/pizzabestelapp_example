package model;

import java.util.ArrayList;
import java.util.Random;

public class Bestelling
{
	private int bestelnr;
	private boolean besteld;
	private Klant klant;
	private ArrayList<Pizza> pizzaList;

	public Bestelling()
	{
		besteld = false;
		pizzaList = new ArrayList<Pizza>();
		Random random = new Random();
		bestelnr = random.nextInt(200000);
	}

	public Klant getKlant()
	{
		return klant;
	}

	public void setKlant(Klant klant)
	{
		this.klant = klant;
	}

	public void addPizza(Pizza pizza)
	{
		pizzaList.add(pizza);
	}

	public void bestel()
	{
		besteld = true;
	}

	public String toString()
	{
		// stap door het lijstje heen van pizza's
		String pizzalistStr = "";
		double totaalprijs = 0.0;
		for (Pizza pizza : pizzaList)
		{
			pizzalistStr += pizza.getNaam() + " -- " + pizza.getPrijs() + "\n";
			totaalprijs += pizza.getPrijs();
		}

		String bestelNummerStr = "";
		if (besteld)
		{
			bestelNummerStr = "Ordernr: " + bestelnr;
		}

		return klant.getNaam() + "\n" + 
			klant.getAdres() + "\n" + 
			klant.getMobielnr() + "\n\n" + 
			pizzalistStr + "\n" + 
			String.format("Totaalbedrag:     %.2f", totaalprijs) + "\n" + 
			bestelNummerStr + "\n";
	}

}
