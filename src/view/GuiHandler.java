package view;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Pizza;

public class GuiHandler
{
	private ArrayList<Pizza> pizzaTypeList;

	private final Label lblName, lblAddress, lblMobile, lblPizza;

	private final TextField txtName;
	private final TextField txtAddress;
	private final TextField txtMobile;

	private final Button btnAdd;
	private final Button btnOrder;
	private final Button btnClear;

	private final ComboBox<Pizza> pizzaComboBox;
	private final TextArea orderTxtArea;

	// constructor
	public GuiHandler(GridPane pane)
	{
				// create pizza typelist
		pizzaTypeList = new ArrayList<Pizza>();
		Pizza pizza = new Pizza("Pizza Margherita", 8.50);
		pizzaTypeList.add(pizza);
		pizza = new Pizza("Pizza Pepperoni", 9.50);
		pizzaTypeList.add(pizza);
		pizza = new Pizza("Pizza Giros", 12.99);
		pizzaTypeList.add(pizza);
		pizza = new Pizza("Pizza Courgette", 11.79);
		pizzaTypeList.add(pizza);
		pizza = new Pizza("Pizza BBQ Pulled Beef", 15.77);
		pizzaTypeList.add(pizza);

		orderTxtArea = new TextArea();
		orderTxtArea.setPrefHeight(300);
		orderTxtArea.setPrefWidth(200);

		ObservableList<Pizza> pizzalist = FXCollections.observableArrayList(pizzaTypeList);

//    	ObservableList<String> pizzalist = 
//    		    FXCollections.observableArrayList(
//    		        "Pizza Margherita         8,50",
//    		        "Pizza Pepperoni          9,50",
//    		        "Pizza Giros             12,99",
//    		        "Pizza Courgette         11,79",
//    		        "Pizza BBQ Pulled Beef   15,77"
//    		    );
//    	
		pizzaComboBox = new ComboBox<Pizza>(pizzalist);

		lblName = new Label("Naam");
		lblAddress = new Label("Adres");
		lblMobile = new Label("Mobiel");
		lblPizza = new Label("Pizza");

		txtName = new TextField();
		txtAddress = new TextField();
		txtMobile = new TextField();

		btnAdd = new Button("Voeg toe");
		btnOrder = new Button("Verzend Bestelling");
		btnClear = new Button("Nieuwe bestelling");
		
		pane.setVgap(5);
		pane.setHgap(8);
		pane.setPadding(new Insets(10, 15, 20, 15));

		pane.add(lblName, 0, 0);
		pane.add(lblAddress, 0, 1);
		pane.add(lblMobile, 0, 2);
		pane.add(lblPizza, 0, 3);

		pane.add(txtName, 1, 0);
		pane.add(txtAddress, 1, 1);
		pane.add(txtMobile, 1, 2);

		pane.add(pizzaComboBox, 1, 3);

		pane.add(btnAdd, 1, 4);
		pane.add(btnOrder, 1, 5);
		pane.add(btnClear, 1, 6);

		pane.add(orderTxtArea, 2, 0, 1, 7);

		
		
	}

	public Button getBtnAdd()
	{
		return btnAdd;
	}

	public Button getBtnOrder()
	{
		return btnOrder;
	}

	public Button getBtnClear()
	{
		return btnClear;
	}

	public ComboBox<Pizza> getPizzaComboBox()
	{
		return pizzaComboBox;
	}

	public TextArea getOrderTxtArea()
	{
		return orderTxtArea;
	}

	public TextField getTxtName()
	{
		return txtName;
	}

	public TextField getTxtAddress()
	{
		return txtAddress;
	}

	public TextField getTxtMobile()
	{
		return txtMobile;
	}

}
