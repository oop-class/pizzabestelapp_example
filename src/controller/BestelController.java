package controller;

import javafx.scene.layout.GridPane;
import model.Bestelling;
import model.Klant;
import model.Pizza;
import view.GuiHandler;

public class BestelController
{
	private GuiHandler gui;
	private Bestelling bestelling;
	
	public BestelController(GridPane pane)
	{
		// create gui
		this.gui = new GuiHandler(pane);
		
		// set actions on gui buttons
		gui.getBtnAdd().setOnAction(event -> addPizza());
		gui.getBtnOrder().setOnAction(event -> bestel());
		gui.getBtnClear().setOnAction(event -> newBestelling());
		
		// maak nieuwe bestelling
		bestelling = new Bestelling();
	}
	
	public void addPizza()
	{
		// zet klant in bestelling, doen we elke keer, nog op te lossen
		Klant klant = new Klant();
		klant.setNaam(gui.getTxtName().getText());
		klant.setAdres(gui.getTxtAddress().getText());
		klant.setMobielnr(gui.getTxtMobile().getText());
		bestelling.setKlant(klant);
		
		// welke pizza is er besteld?
		Pizza pizza = gui.getPizzaComboBox().getValue();
		// voeg deze toe aan de bestelling
		bestelling.addPizza(pizza);
		
		// zet de bestelling in de bestellijst
		gui.getOrderTxtArea().clear();
		gui.getOrderTxtArea().appendText(bestelling.toString());
	}
	
	public void bestel()
	{
		bestelling.bestel();
		// zet de bestelling in de bestellijst
		gui.getOrderTxtArea().clear();
		gui.getOrderTxtArea().appendText(bestelling.toString());		
	}
	
	public void newBestelling()
	{
		bestelling = new Bestelling();
		gui.getOrderTxtArea().clear();
		gui.getTxtName().clear();
		gui.getTxtAddress().clear();
		gui.getTxtMobile().clear();
		gui.getPizzaComboBox().setValue(null);		
	}
	
}
