
import controller.BestelController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class PizzaBestelApp extends Application
{
	
	@Override
	public void start(Stage primaryStage)
	{
		GridPane pane = new GridPane();
		Scene scene = new Scene(pane);

		// de opstartklasse maakt alleen een userController
		// in de constructor wordt de view gestart
		@SuppressWarnings("unused")
		BestelController controller = new BestelController(pane);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Pizza Bestel App - zonder database");

		primaryStage.show();
	}

	public static void main(String[] args)
	{
		launch(args);
	}

}